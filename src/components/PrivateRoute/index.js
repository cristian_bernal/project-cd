import React, { useContext } from "react"
import PropTypes from "prop-types"
import { navigate } from "gatsby"
import { AppContext } from "../../components/context/AppContext"

const PrivateRoute = ({ component: Component, location, ...rest }) => {
  const [isUserLoggedIn] = useContext(AppContext)
  if (!isUserLoggedIn && location.pathname !== `/app/login`) {
    // If we’re not logged in, redirect to the home page.
    navigate(`/app/login`)
    return null
  }

  return <Component {...rest} />
}

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
}

export default PrivateRoute
